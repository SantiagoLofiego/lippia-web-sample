package lippia.web.constants;

public class PhptravelsConstants {

    public static final String DROP_DOWN_MENU_XPATH = "xpath://a[@href='company']";
    public static final String ABOUT_US_BUTTON = "xpath:/html/body/header/div[2]/div/div/div/div/div[2]/nav/ul/li[8]/ul/li[1]/a";
    public static final String ABOUT_US_TITLE = "About Us - PHPTRAVELS";
    public static final String ABOUT_US_URL = "https://phptravels.net/about-us";

    public static final String INPUT_SEARCH_XPATH = "xpath://input[@class='select2-search__field']";
    public static final String INPUT_SEARCH_ACTIVATOR = "css:.select2-selection__arrow";
    public static final String HOTEL_SEARCH_BTN = "id:submit";
    public static final String SEARCH_RESULT_TITLE = "class:sec__title_list";
    public static final String EXPECTED_TITLE = "Search Hotels In Chicago";
    public static final String SEARCHING_VALUE = "chicago";
    public static final String RESULT_SELECTOR_XPATH = "xpath://li[text() = 'Chicago,United States']";
}

