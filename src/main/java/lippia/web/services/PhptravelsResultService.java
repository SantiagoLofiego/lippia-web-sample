package lippia.web.services;

import com.crowdar.core.actions.ActionManager;
import com.crowdar.driver.DriverManager;
import lippia.web.constants.PhptravelsConstants;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class PhptravelsResultService extends ActionManager {


    public static void waitToLoad() {
        WebDriverWait wait = new WebDriverWait(DriverManager.getDriverInstance(), 10L);
        wait.until(d -> d.getCurrentUrl().equals(PhptravelsConstants.ABOUT_US_URL));
    }

    public static void verifyResults() {
        waitToLoad();
        Assert.assertEquals(DriverManager.getDriverInstance().getTitle(), PhptravelsConstants.ABOUT_US_TITLE);
    }

    public static void verivfyHotels() {
        WebElement element = getElement(PhptravelsConstants.SEARCH_RESULT_TITLE);
        Assert.assertEquals(element.getText(), PhptravelsConstants.EXPECTED_TITLE);
    }
}
