package lippia.web.services;

import com.crowdar.core.actions.ActionManager;
import com.crowdar.driver.DriverManager;
import lippia.web.constants.PhptravelsConstants;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import static com.crowdar.core.actions.WebActionManager.navigateTo;

public class PhptravelsHomeService extends ActionManager {

    public static EventFiringWebDriver getDriver() {
        return DriverManager.getDriverInstance();
    }

    public static void navegateAbout() {
        navigateTo("https://phptravels.net");
    }

    public static void clickAboutUsButton() {
        WebElement element = getElement(PhptravelsConstants.DROP_DOWN_MENU_XPATH);
        WebElement btn = getElement(PhptravelsConstants.ABOUT_US_BUTTON);
        Actions actions = new Actions(getDriver());
        actions.moveToElement(element).pause(1000).moveToElement(btn).click(btn).perform();
    }

    public static void navegateHotel() {
        navigateTo("https://phptravels.net/hotels");
    }

    public static void activateComboBox() {
        click(PhptravelsConstants.INPUT_SEARCH_ACTIVATOR);
    }

    public static void enterSearchCriteria() {
        setInput(PhptravelsConstants.INPUT_SEARCH_XPATH, PhptravelsConstants.SEARCHING_VALUE);
    }

    public static void searchHotel() {
        activateComboBox();
        enterSearchCriteria();
        click(PhptravelsConstants.RESULT_SELECTOR_XPATH);
        click(PhptravelsConstants.HOTEL_SEARCH_BTN);
    }

}
