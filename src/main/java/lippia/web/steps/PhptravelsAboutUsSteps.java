package lippia.web.steps;

import com.crowdar.core.PageSteps;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lippia.web.services.PhptravelsHomeService;
import lippia.web.services.PhptravelsResultService;

public class PhptravelsAboutUsSteps extends PageSteps {

    @Given("The user is in home page")
    public void home() {
        PhptravelsHomeService.navegateAbout();
    }

    @When("The user go to About us page")
    public void aboutUs() {
        PhptravelsHomeService.clickAboutUsButton();
    }

    @Then("The \"About us\" page is displayed")
    public void statVerfication() {
        PhptravelsResultService.verifyResults();

    }

}
