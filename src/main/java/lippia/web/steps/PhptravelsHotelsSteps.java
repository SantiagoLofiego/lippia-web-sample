package lippia.web.steps;

import com.crowdar.core.PageSteps;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lippia.web.services.PhptravelsHomeService;
import lippia.web.services.PhptravelsResultService;

public class PhptravelsHotelsSteps extends PageSteps {


    @Given("The user is in Hotels page")
    public void hotels() {
        PhptravelsHomeService.navegateHotel();
    }

    @When("The user search a car between two days")
    public void searchHotel() {
        PhptravelsHomeService.searchHotel();
    }

    @Then("The search page list the available cars in those days.")
    public void viewResult() {
        PhptravelsResultService.verivfyHotels();
    }

}
